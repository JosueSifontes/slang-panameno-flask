from flask import Flask, render_template, request
import redis
r_server = redis.Redis("localhost")

app=Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/agregar', methods=['GET', 'POST'])
def agregar():
    if request.method == 'POST':
        palabra = request.form["palabra"]
        definicion = request.form["definicion"]
        r_server.hset('diccionario', 'palabra', palabra)
        r_server.hset('diccionario', 'definicion', definicion)
        return render_template("despues.html")
    return render_template("agregar.html")

@app.route('/editar', methods=['GET', 'POST'])
def editar():
    if request.method == 'POST':
        palabra = request.form["palabra"]
        r_server.hset('diccionario', 'palabra', palabra)
        return render_template("despues.html")
    return render_template("editar.html")

@app.route('/eliminar', methods=['GET', 'POST'])
def eliminar():
    if request.method == 'POST':
        #palabra = request.form["palabra"]
        r_server.hdel('diccionario', 'palabra')
        r_server.hdel('diccionario', 'definicion')
        return render_template("despues.html")
    return render_template("eliminar.html")

@app.route('/visualizar')
def visualizar():
    if r_server.hexists('diccionario', 'palabra') and r_server.hexists('diccionario','definicion'):
        all = str(r_server.hgetall('diccionario'))
        return f"Los datos son: \n{all}"
    else: 
        return render_template("nodata.html")

@app.route('/buscar')
def buscar():
    if r_server.hexists('diccionario', 'palabra')==True:
        data = str(r_server.hget('diccionario','palabra'))
        data1 = str(r_server.hget('diccionario','definicion'))
        return f"Los datos son: \nPalabra: {data} Definción: {data1}"
    else:
        return render_template("nodata.html")

if __name__ == "__main__":
    app.run(debug=True)